# Generated by Django 3.0.8 on 2020-07-16 22:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainhuerta', '0003_huertaprueba'),
    ]

    operations = [
        migrations.RenameField(
            model_name='huertaprueba',
            old_name='Abono',
            new_name='abono',
        ),
        migrations.RenameField(
            model_name='huertaprueba',
            old_name='Clima',
            new_name='clima',
        ),
        migrations.RenameField(
            model_name='huertaprueba',
            old_name='Necesidad_Agua',
            new_name='necesidad_agua',
        ),
    ]
