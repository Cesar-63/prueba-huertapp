from rest_framework import serializers
from .models import Huertaprueba

class HuertaPruebaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Huertaprueba
        fields = ['id', 'nombre', 'estado_cultivo', 'necesidad_agua', 'clima', 'abono', 'fecha_ingreso']