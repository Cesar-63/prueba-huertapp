from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from .models import Huertaprueba
from .serializers import HuertaPruebaSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


@api_view(["GET"])
@csrf_exempt
def listar_huertaspruebas(request):
    huertaspruebas = Huertaprueba.objects.all()
    serializer = HuertaPruebaSerializer(huertaspruebas, many=True)
    return JsonResponse({'huertas': serializer.data}, safe=False, status=status.HTTP_200_OK)

@api_view(["POST"])
@csrf_exempt
def agregar_huertaprueba(request):
    serializer = HuertaPruebaSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["PUT"])
@csrf_exempt
def actualizar_huertaprueba(request, huertaprueba_id):
    serializer = HuertaPruebaSerializer(data=request.data)
    if serializer.is_valid():#clima abono
        try:
            huertaprueba_item = Huertaprueba.objects.get(id=huertaprueba_id)
            huertaprueba_item.nombre = serializer.validated_data['nombre']
            huertaprueba_item.estado_cultivo = serializer.validated_data['estado_cultivo']
            huertaprueba_item.necesidad_agua = serializer.validated_data['necesidad_agua']
            huertaprueba_item.clima = serializer.validated_data['clima']
            huertaprueba_item.abono = serializer.validated_data['abono']
            huertaprueba_item.save()
            huertaprueba = Huertaprueba.objects.get(id=huertaprueba_id)
            serializer = HuertaPruebaSerializer(huertaprueba)
            return JsonResponse({'huertaprueba': serializer.data}, safe=False, status=status.HTTP_200_OK)
        except ObjectDoesNotExist as e:
            return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
        except Exception:
            return JsonResponse({'error': 'Error interno...'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["DELETE"])
@csrf_exempt
def borrar_huertaprueba(request, huertaprueba_id):
    #user = request.user.id
    try:
        huertaprueba = Huertaprueba.objects.get(id=huertaprueba_id)
        huertaprueba.delete()
        return JsonResponse({'huertaprueba eliminada': huertaprueba_id}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Something went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

#@api_view(["GET"])
#@csrf_exempt
#def listar_huertas(request):
#    huertas = Huerta.objects.all()
#    serializer = HuertaSerializer(huertas, many=True)
#    return JsonResponse({'huertas': serializer.data}, safe=False, status=status.HTTP_200_OK)


@api_view(["GET"])
def vista_inicio(request):
    content = {"cosa": "Bienvenido a Huertapp!"}
    return JsonResponse(content)

