from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.vista_inicio),
    path('listarhuertaspruebas/', views.listar_huertaspruebas),
    path('agregarhuertaprueba/', views.agregar_huertaprueba),
    path('actualizarhuertaprueba/<int:huertaprueba_id>', views.actualizar_huertaprueba),
    path('borrarhuertaprueba/<int:huertaprueba_id>', views.borrar_huertaprueba),
    #path('create/', views.pruebita),
]
